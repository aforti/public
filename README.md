# public

Public images

## gpu-basic-test

Basic test using an tensorflow image with python3. It checks that a GPUs is avaialble 
and runs a minimal code on it.

Can be used when setting up nodes or Panda Resources.

To use it with singularity on the GPU WN

```
singularity -s exec --nv \
docker://gitlab-registry.cern.ch/hepimages/public/gpu-basic-test \
python /test-gpu.py
```

## gpu-atlasml-test

More complicated test. Requires also an input

Can be used when setting up nodes or Panda Resources.

To use it with singularity on the GPU WN

```
singularity exec --nv --pwd /data -B <trainingfile_location>:/data \
docker://gitlab-registry.cern.ch/hepimages/public/gpu-atlasml-test \
python /btagging/DL1_c_vs_b_slim.py trainingfile.h5 10 gpu 50000
```

trainingfile.h5 file can be downloaded from rucio

```
atlasSetup
lsetup rucio
rucio get user.aforti:gpu.basic.training.h5
```
